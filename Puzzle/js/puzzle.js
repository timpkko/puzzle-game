/* TODO: if username and dimension inputted
  1. cancelButton enabled
  2. draw puzzleBoard
  3. start time
*/

function Player(name, dimension, moves, time, theStatus, gameCounter) {
  this.name = name;
  this.dimension = dimension;
  this.moves = moves;
  this.time = time;
  this.theStatus = theStatus;
  this.gameCounter = gameCounter;
}

function Utility() {
  this.playAudio = function() {
    var audio = new Audio("sounds/beep-07.mp3");
    audio.play();
  };

  this.playWinnerAudio = function() {
    var audio = new Audio("sounds/Firework.mp3");
    audio.play();
  };

  this.generateRandomNumber = function(minValue, maxValue) {
    return Math.floor(Math.random() * (maxValue - minValue) + minValue);
  };

  this.playWinnerAudio = function() {
    var audio = new Audio("sounds/Firework.mp3");
    audio.play();
  };

  this.sampleEasyBoardTests = function() {
    var opts = document.getElementById('playerLevelId');
    var opt = opts.options[opts.selectedIndex].value;
    if (opt == "three") {
      return [
        [1, 2, 3],
        [4, 0, 6],
        [7, 5, 8]
      ];
    }
  };

  this.terminateGame = function(theStatus) {
    if (theStatus == "success") {
      new Utility().playWinnerAudio();
    }
    playerManager.storeGameStats(theStatus);
    clearInterval(timer);
    // disable the play and cancel buttons
    var playBtn = document.getElementById('playButton');
    playBtn.disabled = true;
    playBtn.className = "disabledButton";
    var cancelBtn = document.getElementById('cancelButton');
    cancelBtn.disabled = true;
    cancelBtn.className = "disabledButton";
    // reset info
    document.getElementById('playerNameId').value = "";
    document.getElementById('playerLevelId').selectedIndex = 0;
    document.getElementById('nberMoves').textContent = "0";
    var timeTxt = document.querySelectorAll('strong');
    for (var i = 1; i < timeTxt.length; i++) {
      timeTxt[i].textContent = '00';
    }
    document.getElementById('playerNameId').disabled = false;
    document.getElementById('playerLevelId').disabled = false;
  };

  this.cancelPuzzlePlay = function() {
    new Utility().terminateGame("cancelled");
  };

  this.enableButton = function(btnId, theStatus, btnClass) {
    if (theStatus == true) {
      btnId.disabled = false;
      btnId.setAttribute("class", btnClass);
    }
  };

  this.checkFormFilled = function() {
    if (document.getElementById('playerNameId').value != "" &&
      document.getElementById('playerLevelId').selectedIndex > 0) {
      new Utility().enableButton(document.getElementById('playButton'), document.getElementById('playButton').disabled, "greenButton");
    }
  };

  this.showChrono = function() {
    var times = document.getElementsByTagName('strong');
    // reset seconds to 0 if it goes to 60 secs, and increment minutes
    if (times[3].textContent > 58) {
      times[3].textContent = '00';
      times[2].textContent++;
    }
    // reset minutes to 0 if it goes to 60 minutes, and increment hours
    else if (times[2].textContent > 59) {
      times[2].textContent = '00';
      times[1].textContent++;
    }
    // increment seconds
    else {
      times[3].textContent++;
    }
  };

  this.showStats = function() {
    var player = listPlayers[listPlayers.length - 1];
    var playerRow = document.createElement('tr');
    // got through all propreties of player that was pushed in listPlayers
    // and add to the table stats
    for (var p in player) {
      var d = document.createElement('td');
      d.appendChild(document.createTextNode(player[p]));
      playerRow.appendChild(d);
    }
    document.querySelector('.tableStats').appendChild(playerRow);
  };
}

function PlayerManager() {
  this.listPlayers = [];
  this.gameCounter = 1;
  this.nberMoves = 0;

  this.storeGameStats = function(theStatus) {
    var times = document.getElementsByTagName('strong');
    var player = new Player(
      document.getElementById('playerNameId').value,
      document.getElementById('playerLevelId').options[document.getElementById('playerLevelId').selectedIndex].textContent,
      document.getElementById('nberMoves').textContent,
      times[1].textContent + ":" + times[2].textContent + ":" + times[3].textContent,
      theStatus,
      this.gameCounter++
    );
    listPlayers.push(player);

    new Utility().showStats();
    this.nberMoves = 0;
  };
}

function Tile(row, col, tileType, indexNumber, drawnHtmlCell) {
  this.row = row;
  this.col = col;
  this.tileType = tileType; // emptyTile or filledTile
  this.indexNumber = indexNumber; // 0 to puzzleLevel^2 -1
  this.drawnHtmlCell = drawnHtmlCell;

  this.drawIndexNumberInCell = function(index) {
    var div = document.createElement('div');
    div.id = "tile" + this.row + this.col;
    div.className = this.tileType;
    div.appendChild(document.createTextNode(this.indexNumber));
    // check what tile is empty or filled, and addEventListener to appropriate function
    if (this.tileType == "emptyTile") {
      div.addEventListener('click',
        new Utility().playAudio
      );
    } else {
      div.addEventListener('click', function() {
        game.processClickTile(index);
      });
    }
    return div;
  };

  this.resetTileInfo = function(tileType, indexNumber) {
    this.tileType = tileType;
    this.indexNumber = indexNumber;
    this.drawnHtmlCell = this.drawIndexNumberInCell();
  };
}

function PuzzleGame() {
  this.puzzleWidth = document.getElementById('playerLevelId').options[document.getElementById('playerLevelId').selectedIndex].textContent;
  this.puzzleBoard = [];
  this.goalState = [];

  // puzzles 4x4 and 5x5 are not implemented; log error stating it
  if (this.puzzleWidth > 3) {
    console.log(this.puzzleWidth + "x" + this.puzzleWidth + " is not yet fully implemented");
  }

  this.createBoardStructure = function() {
    var solvablePuzzle = true;
    var wholeSize = this.puzzleWidth * this.puzzleWidth;
    // goes through an infinite loop until the puzzle is found to be solvable
    while (solvablePuzzle) {
      // create array of indexes
      var indexes = new Array(wholeSize);
      for (var n = 0; n < indexes.length; n++) {
        var number = new Utility().generateRandomNumber(0, wholeSize);
        if (!indexes.includes(number)) {
          indexes[n] = number;
        } else {
          n--;
        }
      }
      // uses a formula to find the number of inversions:
      // https://www.cs.bham.ac.uk/~mdr/teaching/modules04/java2/TilesSolvability.html
      var checkInversions = 0;
      for (var c = 0; c < indexes.length; c++) {
        for (var d = c; d < indexes.length; d++) {
          if (indexes[c] > 0 && indexes[c] < indexes[d]) {
            checkInversions++;
          }
        }
      }
      // if the puzzle width is odd, number of inversions has to be even;
      // if the puzzle width is even, number of inversions has to be odd;
      if ((checkInversions % 2 == 0 && this.puzzleWidth % 2 != 0) || checkInversions % 2 != 0 && this.puzzleWidth % 2 == 0) {
        solvablePuzzle = false;
      }
    }

    var indexNumbers = [];
    for (var count = 0; count < this.puzzleWidth; count++) {
      indexNumbers.push(indexes.splice(0, this.puzzleWidth));
    }

    // remove comment to use sample instead
    // indexNumbers = new Utility().sampleEasyBoardTests();

    // creating tile objects
    var counter = 0;
    for (var i = 0; i < indexNumbers.length; i++) {
      for (var j = 0; j < indexNumbers[i].length; j++) {
        var whichTile;
        if (indexNumbers[i][j] == 0) {
          whichTile = "emptyTile";
        } else {
          whichTile = "filledTile";
        }
        var tile = new Tile(i, j, whichTile, indexNumbers[i][j]);
        this.puzzleBoard.push(tile);
        tile.drawnHtmlCell = tile.drawIndexNumberInCell(counter);
        counter++;
      }
    }
    this.drawPuzzleBoard();
  };

  this.drawPuzzleBoard = function() {
    var div = document.createElement('div');
    div.className = 'puzzleBoard';
    for (var i = 0; i < this.puzzleBoard.length; i++) {
      this.puzzleBoard[i].drawnHtmlCell = this.puzzleBoard[i].drawIndexNumberInCell(i);
      div.appendChild(this.puzzleBoard[i].drawnHtmlCell);
    }
    // removes any present board
    if (document.getElementsByClassName('puzzleBoard')[0] != undefined) {
      document.getElementsByClassName('puzzleBoard')[0].remove();
    }

    document.getElementById('checkBoardId').appendChild(div);
  };

  this.swap2Tiles = function(indexTile1, indexTile2) {
    var backupType = indexTile1.tileType;
    var backupIndex = indexTile1.indexNumber;
    indexTile1.resetTileInfo(indexTile2.tileType, indexTile2.indexNumber);
    indexTile2.resetTileInfo(backupType, backupIndex);
  };

  this.getNeighboursIndicesArr = function(arrayIndex) {
    var a = [];
    switch (arrayIndex) {
      case 0:
        a = [1, 3, -1, -1];
        break;
      case 1:
        a = [2, 4, 0, -1];
        break;
      case 2:
        a = [-1, 5, 1, -1];
        break;
      case 3:
        a = [4, 6, -1, 0];
        break;
      case 4:
        a = [5, 7, 3, 1];
        break;
      case 5:
        a = [-1, 8, 4, 2];
        break;
      case 6:
        a = [7, -1, -1, 3];
        break;
      case 7:
        a = [8, -1, 6, 4];
        break;
      case 8:
        a = [-1, -1, 7, 5];
        break;
      default:
    }
    return a;
  };

  this.processClickTile = function(arrayIndex) {
    var a = game.getNeighboursIndicesArr(arrayIndex);
    var found = false;
    for (var i = 0; i < a.length; i++) {
      if (a[i] != -1 && this.puzzleBoard[a[i]].indexNumber == 0) {
        found = true;
        this.swap2Tiles(this.puzzleBoard[arrayIndex], this.puzzleBoard[a[i]]);
        document.getElementById('nberMoves').textContent++;
        this.drawPuzzleBoard();
        if (this.computeNumberMisplaced()) {
          new Utility().terminateGame("success");
        }
      }
    }
    // add wrong move sound to tile that can't be moved
    if (!found) {
      document.getElementsByClassName('puzzleBoard')[0].childNodes[arrayIndex].addEventListener('click', new Utility().playAudio);
    }
  };

  this.computeNumberMisplaced = function() {
    for (var i = 0; i < this.puzzleBoard.length - 1; i++) {
      if (this.puzzleBoard[i].indexNumber - 1 != i) {
        return false;
      }
    }
    return true;
  };
}

function mainProgram() {
  var cancelBtn = document.getElementById('cancelButton');
  cancelBtn.disabled = false;
  cancelBtn.setAttribute("class", "redButton");
  game = new PuzzleGame();
  game.createBoardStructure();
  timer = setInterval(new Utility().showChrono, 1000);
  var playButton = document.getElementById('playButton');
  playButton.disabled = true;
  playButton.setAttribute("class", "disabledButton");
  document.getElementById('playerNameId').disabled = true;
  document.getElementById('playerLevelId').disabled = true;
}

function init() {
  document.getElementById('playButton').disabled = true;
  document.getElementById('cancelButton').disabled = true;
  var utilityObj = new Utility();
  document.getElementById('playButton').addEventListener('click', mainProgram);
  document.getElementById('cancelButton').addEventListener('click', utilityObj.cancelPuzzlePlay);
  document.getElementById('playerNameId').addEventListener('input', utilityObj.checkFormFilled);
  document.getElementById('playerLevelId').addEventListener('input', utilityObj.checkFormFilled);
}

var game;
var timer;
var listPlayers = [];
var playerManager = new PlayerManager();
document.addEventListener('DOMContentLoaded', init);
