function showPlayerInfo() {
  var x = document.getElementById("playerInfoContent");
  var x2 = document.getElementById("gameStatsContent");
  x.style.display = "block";
  x2.style.display = "none";
}

function showGameStats() {
  var x = document.getElementById("gameStatsContent");
  var x2 = document.getElementById("playerInfoContent");
  x.style.display = "block";
  x2.style.display = "none";
}

function init() {
  document.getElementById("playerInfoTab").addEventListener("click", showPlayerInfo);
  document.getElementById("GameStatsTab").addEventListener("click", showGameStats);
}

document.addEventListener("DOMContentLoaded", init);
